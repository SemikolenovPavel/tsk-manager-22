package ru.t1.semikolenov.tm.exception.field;

public final class EmptyUserIdException extends AbstractFieldException {

    public EmptyUserIdException() {
        super("Error! User ID is empty...");
    }

}
