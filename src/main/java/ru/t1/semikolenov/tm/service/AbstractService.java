package ru.t1.semikolenov.tm.service;

import ru.t1.semikolenov.tm.api.repository.IRepository;
import ru.t1.semikolenov.tm.api.service.IService;
import ru.t1.semikolenov.tm.enumerated.Sort;
import ru.t1.semikolenov.tm.exception.entity.ModelNotFoundException;
import ru.t1.semikolenov.tm.exception.field.EmptyIdException;
import ru.t1.semikolenov.tm.exception.field.IncorrectIndexException;
import ru.t1.semikolenov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(R repository) {
        this.repository = repository;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort);
    }

    @Override
    public M add(final M model) {
        Optional.ofNullable(model).orElseThrow(ModelNotFoundException::new);
        return repository.add(model);
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public M findOneById(final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional<M> model = Optional.ofNullable(repository.findOneById(id));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        Optional.ofNullable(index).orElseThrow(IncorrectIndexException::new);
        if (index >= repository.getSize()) throw new IncorrectIndexException();
        Optional<M> model = Optional.ofNullable(repository.findOneByIndex(index));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public M remove(final M model) {
        Optional.ofNullable(model).orElseThrow(ModelNotFoundException::new);
        repository.remove(model);
        return model;
    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (index >= repository.getSize()) throw new IncorrectIndexException();
        return repository.removeByIndex(index);
    }

    @Override
    public void removeAll(Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

}
