package ru.t1.semikolenov.tm.service;

import ru.t1.semikolenov.tm.api.service.IAuthService;
import ru.t1.semikolenov.tm.api.service.IUserService;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.exception.entity.UserNotFoundException;
import ru.t1.semikolenov.tm.exception.field.EmptyLoginException;
import ru.t1.semikolenov.tm.exception.field.EmptyPasswordException;
import ru.t1.semikolenov.tm.exception.user.AccessDeniedException;
import ru.t1.semikolenov.tm.exception.user.AuthenticationException;
import ru.t1.semikolenov.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.semikolenov.tm.exception.user.PermissionException;
import ru.t1.semikolenov.tm.model.User;
import ru.t1.semikolenov.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        if (user.getLocked()) throw new AuthenticationException();
        if (!user.getPasswordHash().equals(HashUtil.salt(password))) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findOneById(userId);
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void checkRoles(final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        if (user == null) throw new UserNotFoundException();
        final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
