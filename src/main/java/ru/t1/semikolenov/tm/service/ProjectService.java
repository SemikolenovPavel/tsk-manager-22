package ru.t1.semikolenov.tm.service;

import ru.t1.semikolenov.tm.api.repository.IProjectRepository;
import ru.t1.semikolenov.tm.api.service.IProjectService;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.exception.entity.ModelNotFoundException;
import ru.t1.semikolenov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.semikolenov.tm.exception.field.*;
import ru.t1.semikolenov.tm.model.Project;
import ru.t1.semikolenov.tm.repository.ProjectRepository;

import java.util.Date;
import java.util.Optional;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Optional<Project> project = Optional.ofNullable(repository.create(userId, name));
        return project.orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public Project create(final String userId, final String name, String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        Optional<Project> project = Optional.ofNullable(repository.create(userId, name, description));
        return project.orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public Project create(final String userId, String name, String description, Date dateBegin, Date dateEnd) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final Project project = Optional.ofNullable(create(name, description))
                .orElseThrow(ProjectNotFoundException::new);
        if (dateBegin == null) throw new IncorrectDateBeginException();
        else project.setDateBegin(dateBegin);
        if (dateEnd == null) throw new IncorrectDateEndException();
        else project.setDateEnd(dateEnd);
        project.setUserId(userId);
        return project;
    }

    @Override
    public Project updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return project;
    }

    @Override
    public Project updateById(final String userId, final String id, final String name, final String description, Date dateBegin, Date dateEnd) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        if (dateBegin == null) throw new IncorrectDateBeginException();
        else project.setDateBegin(dateBegin);
        if (dateEnd == null) throw new IncorrectDateEndException();
        else project.setDateEnd(dateEnd);
        project.setUserId(userId);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (index >= repository.getSize()) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String name, final String description, Date dateBegin, Date dateEnd) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (index >= repository.getSize()) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        if (dateBegin == null) throw new IncorrectDateBeginException();
        else project.setDateBegin(dateBegin);
        if (dateEnd == null) throw new IncorrectDateEndException();
        else project.setDateEnd(dateEnd);
        project.setUserId(userId);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String userId, String id, Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        project.setUserId(userId);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final String userId, Integer index, Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (index >= repository.getSize()) throw new IncorrectIndexException();
        final Project project = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        project.setUserId(userId);
        return project;
    }

}
