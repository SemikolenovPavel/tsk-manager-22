package ru.t1.semikolenov.tm.enumerated;

public enum Role {

    ADMIN("Administrator"),
    USUAL("Usual user");

    private String displayName;

    Role(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
