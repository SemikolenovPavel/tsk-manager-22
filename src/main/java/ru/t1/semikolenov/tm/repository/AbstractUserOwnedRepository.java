package ru.t1.semikolenov.tm.repository;

import ru.t1.semikolenov.tm.api.repository.IUserOwnedRepository;
import ru.t1.semikolenov.tm.enumerated.Sort;
import ru.t1.semikolenov.tm.model.AbstractUserOwnedModel;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public List<M> findAll(final String userId) {
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, Sort sort) {
        final Comparator<M> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public M findOneById(final String userId, final String id) {
        return models.stream()
                .filter(m -> id.equals(m.getId()) &&
                        userId.equals(m.getUserId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public int getSize(final String userId) {
        return (int) models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

    @Override
    public M remove(final String userId, final M model) {
        return removeById(userId, model.getId());
    }

    @Override
    public M removeById(final String userId, final String id) {
        final Optional<M> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(models::remove);
        return model.get();
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        final Optional<M> model = Optional.ofNullable(findOneByIndex(userId, index));
        model.ifPresent(models::remove);
        return model.get();
    }

    @Override
    public M add(final String userId, final M model) {
        model.setUserId(userId);
        return add(model);
    }

}
