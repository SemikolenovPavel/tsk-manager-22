package ru.t1.semikolenov.tm.repository;

import ru.t1.semikolenov.tm.api.repository.IUserRepository;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.model.User;
import ru.t1.semikolenov.tm.util.HashUtil;

import java.util.Optional;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User create(String login, String password) {
        final User user = new User();
        user.setRole(Role.USUAL);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return add(user);
    }

    @Override
    public User create(String login, String password, String email) {
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(String login, String password, Role role) {
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        return models.stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return models.stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User removeByLogin(final String login) {
        final Optional<User> user = Optional.ofNullable(findByLogin(login));
        user.ifPresent(models::remove);
        return user.get();
    }

}
