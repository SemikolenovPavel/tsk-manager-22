package ru.t1.semikolenov.tm.api.repository;

import ru.t1.semikolenov.tm.api.model.ICommand;
import ru.t1.semikolenov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Collections;

public interface ICommandRepository {

    Collection<AbstractCommand> getTerminalCommands();

    abstract void add(AbstractCommand command);

    abstract AbstractCommand getCommandByName(String name);

    abstract AbstractCommand getCommandByArgument(String argument);

}
