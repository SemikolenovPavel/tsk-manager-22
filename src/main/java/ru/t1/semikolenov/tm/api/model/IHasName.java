package ru.t1.semikolenov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
