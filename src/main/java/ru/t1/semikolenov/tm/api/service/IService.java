package ru.t1.semikolenov.tm.api.service;

import ru.t1.semikolenov.tm.api.repository.IRepository;
import ru.t1.semikolenov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {


}
