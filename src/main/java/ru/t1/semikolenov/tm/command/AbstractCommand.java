package ru.t1.semikolenov.tm.command;

import ru.t1.semikolenov.tm.api.model.ICommand;
import ru.t1.semikolenov.tm.api.service.IAuthService;
import ru.t1.semikolenov.tm.api.service.IServiceLocator;
import ru.t1.semikolenov.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    public abstract String getName();

    public abstract String getDescription();

    public abstract String getArgument();

    public abstract void execute();

    public abstract Role[] getRoles();

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public String getUserId() {
        return getAuthService().getUserId();
    }

    protected IServiceLocator serviceLocator;

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String description = getDescription();
        final String argument = getArgument();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }
}
