package ru.t1.semikolenov.tm.command.system;

import ru.t1.semikolenov.tm.api.model.ICommand;
import ru.t1.semikolenov.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandsShowCommand extends AbstractSystemCommand {

    public static final String NAME = "commands";

    public static final String DESCRIPTION = "Show commands list.";

    public static final String ARGUMENT = "-cmd";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        System.out.println("TASK MANAGER COMMANDS:");
        for (final ICommand command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
